# encoding:'utf-8'
'''
This simple function is just to tally the number of country
being attacked due to terrorism

TODO

[ ] Please add filtering capabilities to df_tally_multiple method

[ ] Please attach a code from GTD codebook for country
[ ] How to combine GTD and PIRUS person of interest 
[ ] Add a method to find a pattern in a column of a database (regex compatible)
[ ] Creating a bin (should I use density estimation method?)
[ ] Creating a quartile
[ ] GIS related
[ ] save and load functionality for large dataframe
'''
import pandas as pd, numpy as np
import os, sys

def csv2dataframe(filename):
    '''
    Convert csv database to pandas dataframe
    I will use: 
    
    pandas.read_csv(filename)
    
    Please use the absolute path or have 
    the database in the same folder as 
    this filename
    '''
    try:
        return pd.read_csv(filename, low_memory = False)
    except:
        return pd.read_csv(filename)

def excel2dataframe(filename):
    '''
    Not recommending using excel
    to transfer database into dataframe.
    Use CSV file instead
    '''
    try:
        return pd.read_excel(filename)
    except:
        return pd.read_excel(filename, low_memory= False)
   
def single_column_tally(country_column):
    '''
    BUGGY DO NOT USE
    Get the counting of occurance by country
    not for numerical values

    use only one column
    '''
    locals
    return country_column.groupby(country_txt).count()

def df_tally(df,column):
    '''
    Get the counting of occurance by country
    not for numerical values

    use the whole pd.Dataframe.groupby(variable)[variable].count()
    '''
    return df.groupby(column)[column].count()

def df_tally_sorted(df,column):
    '''
    Using pd.Dataframe.value_counts()
    to get a sorted values
    '''
    return df[column].value_counts()

def get_filename(pattern,files=os.listdir('.')):
    '''
    NAIVE APPROACH >>> PLEASE REVISIT FOR
    ERROR HANDLING

    This method is used to 
    get a filename that 
    has a pattern in the 
    string
    Default location is the current directory
    NOT utf-8 coded
    '''
    for file_name in files:
        if pattern in file_name:
            print file_name
            return file_name

def df_tally_timeline(df,variable,time):
    '''
    This is my way of generating timeline
    based a variable.
    
    The function return a pandas.Series object
    it is similar to 2D dictionary data type
    '''
    return df.groupby([variable,time]).size()

def df_tally_multiple(df,var_list):
    '''
    This is a tally generation based on
    multiple variables
    
    df: 
    the dataframe

    var_list:
    a list of variables
    in the desired order. The output will
    follow this order

    I will use df.groupby([var1,var2...]).size()
    to generate the output
    '''
    return df.groupby(var_list).size().reset_index(name="frequency")

def saving_vars(var,var_name,vartype=''):
    '''
    I use this method to save a variable 
    using
    the default saving into numpy npy
    '''

    import pickle
    import numpy as np
    if vartype == 'np':
        np.save(var_name,var)
    else:
        with open(var_name, 'w') as f:
            pickle.dump(var, f)


